<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
		<title>Welcome to MP Opinion Poll</title>
		<meta name="description" content="">
		<meta name="author" content="">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="shortcut icon" href="images/favicon.ico">
                <?php include 'includes/csslinks.php'; ?>
	</head>

	<body class="no-trans front-page transparent-header ">
                <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		       <!-- header-container start -->
                        <?php include './includes/header.php'; ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
				<div class="slideshow">
					
					<!-- slider revolution start -->
					<!-- ================ -->
                                        <div class="slider-banner-container" style="padding-top:40px;">
                                                <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                                                    <div id='full-bg'>
                                                        <div class="container">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-8" id="questionBox">
                                                                 <div class="modal-content">
                                                                <div class="modal-header" id="questionBoxHeader">
                                                                    <h4 class="modal-title" id="myModalLabel">In light of the MP Board Results, Who do you think is responsible for deteriorating performance of students?</h4>
                                                                </div>
                                                                <div class="modal-body" id="optionBox">
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <button class="btn  col-md-12 col-xs-12 radius-50" id="optionOne">School</button> 
                                                                        <button class="btn radius-50 col-md-12 col-xs-12" id="optionOne">Parents</button> 
                                                                        <button class="btn  radius-50 col-md-12 col-xs-12" id="optionOne">Govt. of MP</button> 
                                                                        <button class="btn radius-50 col-md-12 col-xs-12" id="optionOne">Curriculum</button> 
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <p>&nbsp;</p>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                           
                                                        </div>
                                                        
                                                    </div>        
                                                 </section>
					</div>
					<!-- slider revolution end -->

				</div>
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

		
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
                <?php include 'includes/jslinks.php'; ?>
		
	</body>


</html>

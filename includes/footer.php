<div class="subfooter" id="bg-subfooter" >
    
    <div class="bg-fullscreen">
        <div class="container">
            <div class="subfooter-inner">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center" style="color:white;padding:5px;">Copyright © 2017 MP Opinion Poll. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>